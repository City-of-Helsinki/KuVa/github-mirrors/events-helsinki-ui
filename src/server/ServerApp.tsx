import { ApolloClient } from 'apollo-client';
import { i18n as i18nType } from 'i18next';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { I18nextProvider } from 'react-i18next';
import { StaticRouter } from 'react-router-dom';

import App from '../domain/app/App';

export interface StaticContext {
  url?: string;
}

interface Props {
  client: ApolloClient<Record<string, unknown>>;
  context: StaticContext;
  url: string;
  i18n: i18nType;
}

const ServerApp: React.FC<Props> = ({ client, context, url, i18n }) => {
  return (
    <I18nextProvider i18n={i18n}>
      <ApolloProvider client={client}>
        <StaticRouter location={url} context={context}>
          <App />
        </StaticRouter>
      </ApolloProvider>
    </I18nextProvider>
  );
};

export default ServerApp;
